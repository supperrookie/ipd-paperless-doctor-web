import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwipComponent } from './swip.component';

describe('SwipComponent', () => {
  let component: SwipComponent;
  let fixture: ComponentFixture<SwipComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SwipComponent]
    });
    fixture = TestBed.createComponent(SwipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
