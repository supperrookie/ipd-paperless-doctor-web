import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmissionnoteRoutingModule } from './admissionnote-routing.module';
import { AdmissionnoteComponent } from './admissionnote.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';


@NgModule({
  declarations: [
    AdmissionnoteComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    AdmissionnoteRoutingModule,
    NzCheckboxModule,
  ]
})
export class AdmissionnoteModule { }
