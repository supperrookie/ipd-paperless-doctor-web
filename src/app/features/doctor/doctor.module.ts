import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorComponent } from './doctor.component';



import { SharedModule } from 'src/app/shared/shared.module';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import {VariableShareService} from '../../core/services/variable-share.service';



@NgModule({
  declarations: [
    DoctorComponent

  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    DoctorRoutingModule
  ],
  exports:[

  ],
  providers: [VariableShareService]
})
export class DoctorModule { }
