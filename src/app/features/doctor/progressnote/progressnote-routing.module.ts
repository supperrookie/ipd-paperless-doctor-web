import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgressnoteComponent } from './progressnote.component';

const routes: Routes = [{ path: '', component: ProgressnoteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressnoteRoutingModule { }
