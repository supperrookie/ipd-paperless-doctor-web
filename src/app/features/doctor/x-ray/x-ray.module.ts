import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { XRayRoutingModule } from './x-ray-routing.module';
import { XRayComponent } from './x-ray.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    XRayComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    XRayRoutingModule
  ]
})
export class XRayModule { }
