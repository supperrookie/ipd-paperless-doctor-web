import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorComponent } from './doctor.component';
import { nurseWaitingGuard } from '../../core/guard/nurse-waiting.guard';


const routes: Routes = [
  {
    path: '',
    component: DoctorComponent,
    data: {
      breadcrumb: 'ตึกผู้ป่วยใน'
    },
    children: [
      {
        path: '', redirectTo: 'listward', pathMatch: 'full',
        data: {
          breadcrumb: ''
        },
      },
      {
        path: 'main',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule),
        data: {
          breadcrumb: 'ทั่วไป'
        },
      },
      {
        path: 'listward',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./listward/listward.module').then(m => m.ListwardModule),
        data: {
          breadcrumb: ''
        },
      
      },
      {
        path: 'patient-info',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./patientinfo/patientinfo.module').then(m => m.PatientinfoModule),       
        data: {
          breadcrumb: 'Patient Info'
        },
      },
      
      {
        path: 'admissionnote',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./admissionnote/admissionnote.module').then(m => m.AdmissionnoteModule),
        data: {
          breadcrumb: 'Admission-Note'
        },
      },
      {
        path: 'list-patient',
        // canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./listpatient/listpatient.module').then(m => m.ListpatientModule),
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วยใน'
        },
      },
      // {
      //   path: 'doctor-order',
      //   canActivate: [nurseWaitingGuard],
      //   loadChildren: () => import('./doctor-order/doctor-order.module').then(m => m.DoctorOrderModule),
      //   data: {
      //     breadcrumb: 'Doctor-Order'
      //   },
      // },
      {
        path: 'progressnote',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./progressnote/progressnote.module').then(m => m.ProgressnoteModule),
        data: {
          breadcrumb: 'Progress-Note'
        },
      },{
        path: 'standing',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./standing/standing.module').then(m => m.StandingModule),
        data: {
          breadcrumb: 'Standing-Order'
        },
      },{
        path: 'x-ray',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./x-ray/x-ray.module').then(m => m.XRayModule),
        data: {
          breadcrumb: 'X-Ray'
        },
      },{
        path: 'cause-of-death',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./cause-of-death/cause-of-death.module').then(m => m.CauseOfDeathModule),
        data: {
          breadcrumb: 'Cause of death'
        },
      },{
        path: 'consult',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./consult/consult.module').then(m => m.ConsultModule),
        data: {
          breadcrumb: 'Consult'
        },
      },{
        path: 'discharge',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./discharge/discharge.module').then(m => m.DischargeModule),
        data: {
          breadcrumb: 'Discharge'
        },
      },{
        path: 'ekg',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./ekg/ekg.module').then(m => m.EkgModule),
        data: {
          breadcrumb: 'Ekg'
        },
      },{
        path: 'follow-up',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./follow-up/follow-up.module').then(m => m.FollowUpModule),
        data: {
          breadcrumb: 'Follow up'
        },
      },{
        path: 'home-medication',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./home-medication/home-medication.module').then(m => m.HomeMedicationModule),
        data: {
          breadcrumb: 'Home Medication'
        },
      },{
        path: 'nurse-note',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./nurse-note/nurse-note.module').then(m => m.NurseNoteModule),
        data: {
          breadcrumb: 'Nurse note'
        },
      },{
        path: 'refer',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./refer/refer.module').then(m => m.ReferModule),
        data: {
          breadcrumb: 'Refer'
        },
      },{
        path: 'vital-sign',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./vital-sign/vital-sign.module').then(m => m.VitalSignModule),
        data: {
          breadcrumb: 'Vital sign'
        },
      },



      
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
