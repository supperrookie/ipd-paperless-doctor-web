import { Component } from '@angular/core';
import { UserProfileService } from '../../core/services/user-profiles.service';
import { UserProfileApiService } from '../login/services/user-profile.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-nurse',
  templateUrl: './nurse.component.html',
  styleUrls: ['./nurse.component.css']
})
export class NurseComponent {
  jwtHelper: JwtHelperService = new JwtHelperService();
  user_login_name: any;
  user_id: any

  constructor(

    private userProfileService: UserProfileService,
    private userProfileApiService: UserProfileApiService
  ) {
    const token: any = sessionStorage.getItem('token')
    const decoded = this.jwtHelper.decodeToken(token);
    this.user_login_name = this.userProfileService.fname;
    // this.user_id = decoded.sub;
    // this.getUserById();
  }

  

}
